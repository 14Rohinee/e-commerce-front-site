import { Suspense, lazy } from "react";
import ScrollToTop from "./helpers/scroll-top";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useEffect } from "react";
import { checkAuthTokenValidity } from "../src/utils/authUtils";
import ProtectedRoute from "./utils/ProtectedRoute";

// home pages
const Home = lazy(() => import("./pages/home/Home"));

// shop pages
const ShopGridStandard = lazy(() => import("./pages/shop/ShopGridStandard"));

// product pages
const Product = lazy(() => import("./pages/shop-product/Product"));

// other pages
const About = lazy(() => import("./pages/other/About"));
const Contact = lazy(() => import("./pages/other/Contact"));
const MyAccount = lazy(() => import("./pages/other/MyAccount"));
const Login = lazy(() => import("./pages/other/Login"));
const Register = lazy(() => import("./pages/other/Register"));
const Cart = lazy(() => import("./pages/other/Cart"));
const Wishlist = lazy(() => import("./pages/other/Wishlist"));
const Compare = lazy(() => import("./pages/other/Compare"));
const Checkout = lazy(() => import("./pages/other/Checkout"));
const Payment = lazy(() => import("./pages/other/Payment"));
const NotFound = lazy(() => import("./pages/other/NotFound"));

// Create a sessionId for the user if it doesn't exist. sessionId must also have a today's timestamp to make sure it has a unique value.
if (!localStorage.getItem("sessionId")) {
    localStorage.setItem(
        "sessionId",
        Math.random().toString(36).substring(7) + Date.now()
    );
}

const App = () => {
    // Check if the user is logged in (you should implement this logic)
    const isLoggedIn = checkAuthTokenValidity();

    // Check token validity on component mount
    useEffect(() => {
        if (!isLoggedIn) {
            // Remove token from local storage
            localStorage.removeItem("loginToken");
            localStorage.removeItem("loggedInUser");
        }
    }, []);

    return (
        <Router>
            <ScrollToTop>
                <Suspense
                    fallback={
                        <div className="flone-preloader-wrapper">
                            <div className="flone-preloader">
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    }
                >
                    <Routes>
                        <Route
                            path={process.env.PUBLIC_URL + "/"}
                            element={<Home />}
                        />

                        {/* Shop pages */}
                        <Route
                            path={process.env.PUBLIC_URL + "/shop"}
                            element={<ShopGridStandard />}
                        />

                        {/* Shop product pages */}
                        <Route
                            path={process.env.PUBLIC_URL + "/product/:slug"}
                            element={<Product />}
                        />

                        {/* Other pages */}
                        <Route
                            path={process.env.PUBLIC_URL + "/about"}
                            element={<About />}
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/contact"}
                            element={<Contact />}
                        />

                        <Route
                            path={process.env.PUBLIC_URL + "/login"}
                            element={<Login />}
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/register"}
                            element={<Register />}
                        />

                        <Route
                            path={process.env.PUBLIC_URL + "/cart"}
                            element={<Cart />}
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/wishlist"}
                            element={<Wishlist />}
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/compare"}
                            element={<Compare />}
                        />

                        {/* Protected routes */}
                        <Route
                            path={process.env.PUBLIC_URL + "/my-account"}
                            element={
                                <ProtectedRoute
                                    isLoggedIn={isLoggedIn}
                                    redirectTo="my-account"
                                >
                                    <MyAccount />
                                </ProtectedRoute>
                            }
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/checkout"}
                            element={
                                <ProtectedRoute
                                    isLoggedIn={isLoggedIn}
                                    redirectTo="checkout"
                                >
                                    <Checkout />
                                </ProtectedRoute>
                            }
                        />
                        <Route
                            path={process.env.PUBLIC_URL + "/payment"}
                            element={
                                <ProtectedRoute
                                    isLoggedIn={isLoggedIn}
                                    redirectTo="payment"
                                >
                                    <Payment />
                                </ProtectedRoute>
                            }
                        />
                        {/* Protected routes */}

                        <Route path="*" element={<NotFound />} />
                    </Routes>
                </Suspense>
            </ScrollToTop>
        </Router>
    );
};

export default App;
