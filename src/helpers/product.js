export const APP_URL =
    process.env.REACT_APP_ENV === "live"
        ? process.env.REACT_APP_API_BASE_URL_LIVE_FRONTEND
        : process.env.REACT_APP_API_BASE_URL_LOCAL_FRONTEND;

// get products
export const getProducts = (products, category = null, type, limit) => {
    const finalProducts = products;

    if (type && type === "new") {
        const newProducts = finalProducts.filter((single) => single.isNew);
        return newProducts.slice(0, limit ? limit : newProducts.length);
    }

    if (type && type === "bestSeller") {
        return finalProducts
            .sort((a, b) => {
                return b.saleCount - a.saleCount;
            })
            .slice(0, limit ? limit : finalProducts.length);
    }

    if (type && type === "saleItems") {
        const saleItems = finalProducts.filter(
            (single) => single.discount && single.discount > 0
        );
        return saleItems.slice(0, limit ? limit : saleItems.length);
    }

    return finalProducts.slice(0, limit ? limit : finalProducts.length);
};

// get product discount price
export const getDiscountPrice = (price, discount) => {
    return discount && discount > 0 ? price - price * (discount / 100) : null;
};

// get product cart quantity
export const getProductCartQuantity = (cartItems, product, color, size) => {
    let productInCart = cartItems.find(
        (single) =>
            single.id === product.id &&
            (single.selectedProductColor
                ? single.selectedProductColor === color
                : true) &&
            (single.selectedProductSize
                ? single.selectedProductSize === size
                : true)
    );
    if (cartItems.length >= 1 && productInCart) {
        if (product.variation) {
            return cartItems.find(
                (single) =>
                    single.id === product.id &&
                    single.selectedProductColor === color &&
                    single.selectedProductSize === size
            ).quantity;
        } else {
            return cartItems.find((single) => product.id === single.id)
                .quantity;
        }
    } else {
        return 0;
    }
};

export const cartItemStock = (item, color, size) => {
    if (item.stock) {
        return item.stock;
    } else {
        return item.variation
            .filter((single) => single.color === color)[0]
            .size.filter((single) => single.name === size)[0].stock;
    }
};

//get products based on category
export const getSortedProducts = (
    products,
    sortType,
    sortValue,
    searchText
) => {
    let newProducts = [...products];

    if (sortType && sortValue) {
        if (sortType === "category") {
            newProducts = products.filter(
                (product) => product.category.slug === sortValue
            );
        }
        if (sortType === "tag") {
            newProducts = products.filter(
                (product) =>
                    product.tags.filter((single) => single === sortValue)[0]
            );
        }
        if (sortType === "filterSort") {
            if (sortValue === "priceHighToLow") {
                newProducts = newProducts.sort((a, b) => b.price - a.price);
            }
            if (sortValue === "priceLowToHigh") {
                newProducts = newProducts.sort((a, b) => a.price - b.price);
            }
        }
    }

    // Apply search text filter
    if (searchText) {
        newProducts = newProducts.filter((product) =>
            product.name.toLowerCase().includes(searchText.toLowerCase())
        );
    }

    return newProducts;
};

// get individual element
const getIndividualItemArray = (array) => {
    let individualItemArray = array.filter(function (v, i, self) {
        return i === self.indexOf(v);
    });
    return individualItemArray;
};

// get individual categories
export const getIndividualCategories = (products) => {
    let productCategories = [];
    products &&
        products.map((product) => {
            return (
                product.category &&
                product.category.map((single) => {
                    return productCategories.push(single);
                })
            );
        });
    const individualProductCategories =
        getIndividualItemArray(productCategories);
    return individualProductCategories;
};

// get individual tags
export const getIndividualTags = (products) => {
    let productTags = [];
    products &&
        products.map((product) => {
            return (
                product.tags &&
                product.tags.map((single) => {
                    return productTags.push(single);
                })
            );
        });
    const individualProductTags = getIndividualItemArray(productTags);
    return individualProductTags;
};

// get individual colors
export const getIndividualColors = (products) => {
    let productColors = [];
    products &&
        products.map((product) => {
            return (
                product.variation &&
                product.variation.map((single) => {
                    return productColors.push(single.color);
                })
            );
        });
    const individualProductColors = getIndividualItemArray(productColors);
    return individualProductColors;
};

// get individual sizes
export const getProductsIndividualSizes = (products) => {
    let productSizes = [];
    products &&
        products.map((product) => {
            return (
                product.variation &&
                product.variation.map((single) => {
                    return single.size.map((single) => {
                        return productSizes.push(single.name);
                    });
                })
            );
        });
    const individualProductSizes = getIndividualItemArray(productSizes);
    return individualProductSizes;
};

// get product individual sizes
export const getIndividualSizes = (product) => {
    let productSizes = [];
    product.variation &&
        product.variation.map((singleVariation) => {
            return (
                singleVariation.size &&
                singleVariation.size.map((singleSize) => {
                    return productSizes.push(singleSize.name);
                })
            );
        });
    const individualSizes = getIndividualItemArray(productSizes);
    return individualSizes;
};

export const setActiveSort = (e) => {
    const filterButtons = document.querySelectorAll(
        ".sidebar-widget-list-left button, .sidebar-widget-tag button, .product-filter button"
    );
    filterButtons.forEach((item) => {
        item.classList.remove("active");
    });
    e.currentTarget.classList.add("active");
};

export const setActiveLayout = (e) => {
    const gridSwitchBtn = document.querySelectorAll(".shop-tab button");
    gridSwitchBtn.forEach((item) => {
        item.classList.remove("active");
    });
    e.currentTarget.classList.add("active");
};

export const toggleShopTopFilter = (e) => {
    const shopTopFilterWrapper = document.querySelector(
        "#product-filter-wrapper"
    );
    shopTopFilterWrapper.classList.toggle("active");
    if (shopTopFilterWrapper.style.height) {
        shopTopFilterWrapper.style.height = null;
    } else {
        shopTopFilterWrapper.style.height =
            shopTopFilterWrapper.scrollHeight + "px";
    }
    e.currentTarget.classList.toggle("active");
};
