import PropTypes from "prop-types";
import clsx from "clsx";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SectionTitle from "../../components/section-title/SectionTitle";
import ProductGrid from "./ProductGrid";
import axios from "axios";
import { useEffect, useState } from "react";
import { APP_URL } from "../../helpers/product";

const TabProduct = ({
    spaceTopClass,
    spaceBottomClass,
    bgColorClass,
    category,
}) => {
    const [categories, setCategories] = useState();
    const [defaultActiveKey, setDefaultActiveKey] = useState();

    // GetCategories
    const getCategories = () => {
        axios
            .get(`${APP_URL}/get-categories-and-its-products`)
            .then((res) => {
                setCategories(res.data);
                setDefaultActiveKey(res.data[0].slug);
            })
            .catch((err) => console.log(err));
    };

    useEffect(() => {
        getCategories();
    }, []);

    return (
        <div
            className={clsx(
                "product-area",
                spaceTopClass,
                spaceBottomClass,
                bgColorClass
            )}
        >
            <div className="container">
                <SectionTitle
                    titleText="Categories"
                    positionClass="text-center"
                />
                <Tab.Container defaultActiveKey={defaultActiveKey ?? "laptop"}>
                    <Nav
                        variant="pills"
                        className="product-tab-list pt-30 pb-55 text-center"
                    >
                        {categories &&
                            categories.map((category, index) => {
                                return (
                                    <Nav.Item key={index}>
                                        <Nav.Link eventKey={category.slug}>
                                            <h4>{category.name}</h4>
                                        </Nav.Link>
                                    </Nav.Item>
                                );
                            })}
                    </Nav>
                    <Tab.Content>
                        {categories &&
                            categories.map((category, index) => {
                                return (
                                    <Tab.Pane
                                        key={index}
                                        eventKey={category.slug}
                                    >
                                        <div className="row">
                                            <ProductGrid
                                                category={category.name}
                                                type="new"
                                                limit={8}
                                                spaceBottomClass="mb-25"
                                                products={category.products}
                                            />
                                        </div>
                                    </Tab.Pane>
                                );
                            })}

                        {/* <Tab.Pane eventKey="newArrival">
                            <div className="row">
                                <ProductGrid
                                    category={category}
                                    type="new"
                                    limit={8}
                                    spaceBottomClass="mb-25"
                                />
                            </div>
                        </Tab.Pane>
                        <Tab.Pane eventKey="bestSeller">
                            <div className="row">
                                <ProductGrid
                                    category={category}
                                    type="bestSeller"
                                    limit={8}
                                    spaceBottomClass="mb-25"
                                />
                            </div>
                        </Tab.Pane>
                        <Tab.Pane eventKey="saleItems">
                            <div className="row">
                                <ProductGrid
                                    category={category}
                                    type="saleItems"
                                    limit={8}
                                    spaceBottomClass="mb-25"
                                />
                            </div>
                        </Tab.Pane> */}
                    </Tab.Content>
                </Tab.Container>
            </div>
        </div>
    );
};

TabProduct.propTypes = {
    bgColorClass: PropTypes.string,
    category: PropTypes.string,
    spaceBottomClass: PropTypes.string,
    spaceTopClass: PropTypes.string,
};

export default TabProduct;
