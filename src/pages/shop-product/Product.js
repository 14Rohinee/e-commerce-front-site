import { useEffect, useState } from "react";
import { useParams, useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import RelatedProductSlider from "../../wrappers/product/RelatedProductSlider";
import ProductDescriptionTab from "../../wrappers/product/ProductDescriptionTab";
import ProductImageDescription from "../../wrappers/product/ProductImageDescription";
import axios from "axios";
import { APP_URL } from "../../helpers/product";

const Product = () => {
  let { pathname } = useLocation();
  let { slug } = useParams();
  const [product, setProduct] = useState({});

  // Get product by ID
  const getProductById = async () => {
    try {
      const response = await axios.get(`${APP_URL}/get-product-by-slug/${slug}`);
      setProduct(response.data);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    getProductById();
  }, []);

  return (
    <>
      <SEO
        titleTemplate="Product Page"
        description="Product page of flone react minimalist eCommerce template."
      />

      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            {label: "Home", path: process.env.PUBLIC_URL + "/" },
            {label: product.name, path: process.env.PUBLIC_URL + pathname }
          ]}
        />

        {
          product ? (
            <>
              <ProductImageDescription
                spaceTopClass="pt-100"
                spaceBottomClass="pb-100"
                product={product}
              />

              <ProductDescriptionTab
                spaceBottomClass="pb-90"
                product={product}
              />

              <RelatedProductSlider
                spaceBottomClass="pb-95"
                category={product.category}
              />
            </>
          ) : ''
        }

      </LayoutOne>
    </>
  );
};


export default Product;
