import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Accordion from "react-bootstrap/Accordion";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import AddressModal from "../../components/address/AddressModal";
import Swal from "sweetalert2";
import cogoToast from "cogo-toast";
import axios from "axios";
import { APP_URL } from "../../helpers/product";

const MyAccount = () => {
    let { pathname } = useLocation();
    const [userDetails, setUserDetails] = useState({
        id: null,
        name: "",
        email: "",
        mobile: "",
        dob: "",
        password: "",
        confirmPassword: "",
    });
    const [isAddressModalOpen, setIsAddressModalOpen] = useState(false);
    const [shippingAddresses, setShippingAddresses] = useState([]);
    const [editAddressId, setEditAddressId] = useState(null);

    // Get user details
    const getAccountDetails = () => {
        // Get user details from database with axios
        const user = JSON.parse(localStorage.getItem("loggedInUser"));

        setUserDetails((prevState) => ({
            ...prevState,
            id: user._id,
            name: user.name,
            email: user.email,
            mobile: user.mobile,
            dob: user.dob,
        }));
    };

    // Update account details
    const updateAccountDetails = async (e) => {
        e.preventDefault();
        const user = JSON.parse(localStorage.getItem("loggedInUser"));

        // Remove password and confirmPassword from userDetails
        delete userDetails.password;
        delete userDetails.confirmPassword;

        try {
            const response = await axios.put(
                `${APP_URL}/users/update/${user._id}`,
                userDetails
            );
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });
            localStorage.setItem(
                "loggedInUser",
                JSON.stringify(response.data.data.user)
            );
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    // Update password
    const updatePassword = async (e) => {
        e.preventDefault();
        const user = JSON.parse(localStorage.getItem("loggedInUser"));

        // Check if password and confirmPassword are not empty
        if (userDetails.password === "" || userDetails.confirmPassword === "") {
            cogoToast.error("Password and Confirm Password cannot be empty", {
                position: "bottom-left",
            });
            return;
        }
        // Check if password and confirmPassword are same
        else if (userDetails.password !== userDetails.confirmPassword) {
            cogoToast.error("Password and Confirm Password do not match", {
                position: "bottom-left",
            });
            return;
        }
        // Password must be at least 6 characters long containing at least one number and one special character
        else if (
            !userDetails.password.match(
                /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/
            )
        ) {
            cogoToast.error(
                "Password must be at least 6 characters long containing at least one number and one special character",
                { position: "bottom-left" }
            );
            return;
        }

        try {
            const response = await axios.put(
                `${APP_URL}/users/update/${user._id}`,
                userDetails
            );
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });
            localStorage.setItem(
                "loggedInUser",
                JSON.stringify(response.data.data.user)
            );
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    // Get addresses
    const getAddresses = async () => {
        const user = JSON.parse(localStorage.getItem("loggedInUser"));

        try {
            const response = await axios.get(
                `${APP_URL}/shipping-addresses/${user._id}`
            );
            setShippingAddresses(response.data.data.result);
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    // onAddressDelete
    const onAddressDelete = (addressId) => {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const response = await axios.delete(
                        `${APP_URL}/shipping-addresses/delete/${addressId}`
                    );
                    getAddresses();
                    cogoToast.success(response.data.message, {
                        position: "bottom-left",
                    });
                } catch (error) {
                    cogoToast.error(error.response.data.message, {
                        position: "bottom-left",
                    });
                }
            }
        });
    };

    // Add new address
    const addNewAddress = (e) => {
        e.preventDefault();
        setEditAddressId(null);
        setIsAddressModalOpen(true);
    };

    // Edit address
    const editAddress = (addressId) => {
        setEditAddressId(addressId);
        setIsAddressModalOpen(true);
    };

    useEffect(() => {
        getAccountDetails();
        getAddresses();
    }, []);

    const closeAddressModal = () => {
        getAddresses();
        setIsAddressModalOpen(false);
    }

    return (
        <>
            <SEO
                titleTemplate="My Account"
                description="My Account page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "My Account",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />

                <div className="myaccount-area pb-80 pt-100">
                    <div className="container">
                        <div className="row">
                            <div className="ms-auto me-auto col-lg-9">
                                <div className="myaccount-wrapper">
                                    <Accordion defaultActiveKey="0">
                                        <Accordion.Item
                                            eventKey="0"
                                            className="single-my-account mb-20"
                                        >
                                            <Accordion.Header className="panel-heading">
                                                <span>1 .</span> Edit your
                                                account information{" "}
                                            </Accordion.Header>
                                            <Accordion.Body>
                                                <div className="myaccount-info-wrapper">
                                                    <div className="account-info-wrapper">
                                                        <h4>
                                                            My Account
                                                            Information
                                                        </h4>
                                                        <h5>
                                                            Your Personal
                                                            Details
                                                        </h5>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 col-md-6">
                                                            <div className="billing-info">
                                                                <label>
                                                                    Name
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    name="name"
                                                                    value={
                                                                        userDetails.name
                                                                    }
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                name: e
                                                                                    .target
                                                                                    .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="billing-info">
                                                                <label>
                                                                    Email
                                                                    Address
                                                                </label>
                                                                <input
                                                                    type="email"
                                                                    name="email"
                                                                    value={
                                                                        userDetails.email
                                                                    }
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                email: e
                                                                                    .target
                                                                                    .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6">
                                                            <div className="billing-info">
                                                                <label>
                                                                    Mobile
                                                                    Number
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    name="mobile"
                                                                    value={
                                                                        userDetails.mobile
                                                                    }
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                mobile: e
                                                                                    .target
                                                                                    .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6">
                                                            <div className="billing-info">
                                                                <label>
                                                                    DOB
                                                                </label>
                                                                <input
                                                                    type="date"
                                                                    name="dob"
                                                                    value={
                                                                        userDetails.dob
                                                                    }
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                dob: e
                                                                                    .target
                                                                                    .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="billing-back-btn">
                                                        <div className="billing-btn">
                                                            <button
                                                                type="button"
                                                                onClick={
                                                                    updateAccountDetails
                                                                }
                                                            >
                                                                Continue
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Accordion.Body>
                                        </Accordion.Item>

                                        <Accordion.Item
                                            eventKey="1"
                                            className="single-my-account mb-20"
                                        >
                                            <Accordion.Header className="panel-heading">
                                                <span>2 .</span> Change your
                                                password
                                            </Accordion.Header>
                                            <Accordion.Body>
                                                <div className="myaccount-info-wrapper">
                                                    <div className="account-info-wrapper">
                                                        <h4>Change Password</h4>
                                                        <h5>Your Password</h5>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12">
                                                            <div className="billing-info">
                                                                <label>
                                                                    New Password
                                                                </label>
                                                                <input
                                                                    type="password"
                                                                    autoComplete="new-password"
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                password:
                                                                                    e
                                                                                        .target
                                                                                        .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-12 col-md-12">
                                                            <div className="billing-info">
                                                                <label>
                                                                    Password
                                                                    Confirm
                                                                </label>
                                                                <input
                                                                    type="password"
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setUserDetails(
                                                                            {
                                                                                ...userDetails,
                                                                                confirmPassword:
                                                                                    e
                                                                                        .target
                                                                                        .value,
                                                                            }
                                                                        )
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="billing-back-btn">
                                                        <div className="billing-btn">
                                                            <button
                                                                type="button"
                                                                onClick={
                                                                    updatePassword
                                                                }
                                                            >
                                                                Continue
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Accordion.Body>
                                        </Accordion.Item>

                                        <Accordion.Item
                                            eventKey="2"
                                            className="single-my-account mb-20"
                                        >
                                            <Accordion.Header className="panel-heading">
                                                <span>3 .</span> Modify your
                                                address book entries
                                            </Accordion.Header>
                                            <Accordion.Body>
                                                <div className="myaccount-info-wrapper">
                                                    <div className="account-info-wrapper flex-left-right-container">
                                                        <h4 className="flex-left-right-container-item">
                                                            Address Book Entries
                                                        </h4>
                                                        <button
                                                            className="primary-btn"
                                                            onClick={
                                                                addNewAddress
                                                            }
                                                        >
                                                            Add New Address
                                                        </button>
                                                    </div>

                                                    {shippingAddresses &&
                                                        shippingAddresses.map(
                                                            (
                                                                address,
                                                                index
                                                            ) => (
                                                                <div
                                                                    className="entries-wrapper"
                                                                    key={index}
                                                                >
                                                                    <div className="row">
                                                                        <div className="col-lg-6 col-md-6 d-flex align-items-center justify-content-center">
                                                                            <div className="entries-info text-center">
                                                                                <p>
                                                                                    {
                                                                                        address.name
                                                                                    }
                                                                                </p>
                                                                                <p>
                                                                                    {
                                                                                        address.phone
                                                                                    }
                                                                                </p>
                                                                                <p>
                                                                                    {
                                                                                        address.address
                                                                                    }
                                                                                </p>
                                                                                <p>
                                                                                    {
                                                                                        address.landmark
                                                                                    }
                                                                                </p>
                                                                                <p>
                                                                                    {`${address.city}, ${address.state}, ${address.country}, ${address.zip}`}
                                                                                </p>
                                                                                {address.isDefaultAddress && (
                                                                                    <p>
                                                                                        <label className="badge bg-secondary">
                                                                                            Default
                                                                                        </label>
                                                                                    </p>
                                                                                )}
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-lg-6 col-md-6 d-flex align-items-center justify-content-center">
                                                                            <div className="entries-edit-delete text-center">
                                                                                <button
                                                                                    className="edit"
                                                                                    onClick={() => {
                                                                                        editAddress(
                                                                                            address._id
                                                                                        );
                                                                                    }}
                                                                                >
                                                                                    Edit
                                                                                </button>

                                                                                {
                                                                                    address.isDefaultAddress ? (
                                                                                        <button
                                                                                        disabled={true}
                                                                                >
                                                                                    Delete
                                                                                </button>
                                                                                    ) : (
                                                                                        <button
                                                                                    onClick={() => onAddressDelete(address._id)}
                                                                                >
                                                                                    Delete
                                                                                </button>
                                                                                    )
                                                                                }

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        )}

                                                    <div className="billing-back-btn">
                                                        <div className="billing-btn">
                                                            <button type="submit">
                                                                Continue
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <AddressModal
                    addressId={editAddressId}
                    show={isAddressModalOpen}
                    onHide={closeAddressModal}
                />
            </LayoutOne>
        </>
    );
};

export default MyAccount;
