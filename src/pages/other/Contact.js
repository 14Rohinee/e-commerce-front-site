import { useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from "axios";
import { useEffect, useState } from "react";
import { APP_URL } from "../../helpers/product";

const Contact = () => {
  let { pathname } = useLocation();
  const [companyDetails, setCompanyDetails] = useState({});
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    subject: "",
    message: ""
  });

  // Get company details
  const getCompanyDetails = async () => {
    try {
        const response = await axios.get(`${APP_URL}/get-company-details`);
        setCompanyDetails(response.data.data);
    } catch (error) {
        console.log(error.response.data.message);
    }
  };

  // handleFormSubmit
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(`${APP_URL}/get-in-touch`, formData);
      console.log(response.data.message);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  useEffect(() => {
    getCompanyDetails();
  }, []);

  return (
    <>
      <SEO
        titleTemplate="Contact"
        description="Contact page of flone react minimalist eCommerce template."
      />
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            {label: "Home", path: process.env.PUBLIC_URL + "/" },
            {label: "Contact", path: process.env.PUBLIC_URL + pathname }
          ]}
        />
        <div className="contact-area pt-100 pb-100">
          <div className="container">
            <div className="custom-row-2">
              <div className="col-12 col-lg-4 col-md-5">
                <div className="contact-info-wrap">
                  <div className="single-contact-info">
                    <div className="contact-icon">
                      <i className="fa fa-phone" />
                    </div>
                    <div className="contact-info-dec">
                      <p>
                        <a href={`${companyDetails.phone}`}>
                          {companyDetails.phone}
                        </a>
                      </p>
                    </div>
                  </div>
                  <div className="single-contact-info">
                    <div className="contact-icon">
                      <i className="fa fa-globe" />
                    </div>
                    <div className="contact-info-dec">
                      <p>
                        {
                          <a href={`mailto:${companyDetails.email}`}>
                            {companyDetails.email}
                          </a>
                        }
                      </p>
                    </div>
                  </div>
                  <div className="single-contact-info">
                    <div className="contact-icon">
                      <i className="fa fa-map-marker" />
                    </div>
                    <div className="contact-info-dec">
                      {
                        <p className="office-address">
                          {companyDetails.address}
                        </p>
                      }
                    </div>
                  </div>
                  <div className="contact-social text-center">
                    <h3>Follow Us</h3>
                    <ul>
                      <li>
                        <a href="//facebook.com">
                          <i className="fa fa-facebook" />
                        </a>
                      </li>
                      <li>
                        <a href="//pinterest.com">
                          <i className="fa fa-pinterest-p" />
                        </a>
                      </li>
                      <li>
                        <a href="//thumblr.com">
                          <i className="fa fa-tumblr" />
                        </a>
                      </li>
                      <li>
                        <a href="//vimeo.com">
                          <i className="fa fa-vimeo" />
                        </a>
                      </li>
                      <li>
                        <a href="//twitter.com">
                          <i className="fa fa-twitter" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-8 col-md-7">
                <div className="contact-form">
                  <div className="contact-title mb-30">
                    <h2>Get In Touch</h2>
                  </div>
                  <form className="contact-form-style" onSubmit={handleFormSubmit}>
                    <div className="row">
                      <div className="col-lg-6">
                        <input
                          type="text"
                          name="name"
                          placeholder="Name*"
                          onChange={(e) => setFormData({ ...formData, name: e.target.value })}
                          defaultValue={formData.name}
                        />
                      </div>
                      <div className="col-lg-6">
                        <input
                          type="email"
                          name="email"
                          placeholder="Email*"
                          onChange={(e) => setFormData({ ...formData, email: e.target.value })}
                          defaultValue={formData.email}
                        />
                      </div>
                      <div className="col-lg-12">
                        <input
                          name="subject"
                          placeholder="Subject*"
                          type="text"
                          onChange={(e) => setFormData({ ...formData, subject: e.target.value })}
                          defaultValue={formData.subject}
                        />
                      </div>
                      <div className="col-lg-12">
                        <textarea
                          name="message"
                          placeholder="Your Message*"
                          defaultValue={formData.message}
                          onChange={(e) => setFormData({ ...formData, message: e.target.value })}
                        />
                        <button className="submit" type="submit">
                          SEND
                        </button>
                      </div>
                    </div>
                  </form>
                  <p className="form-message" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </>
  );
};

export default Contact;
