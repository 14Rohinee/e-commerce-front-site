import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { getDiscountPrice } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from "axios";
import { useState, useEffect } from "react";
import cogoToast from "cogo-toast";
import { APP_URL } from "../../helpers/product";
import useRazorpay from "react-razorpay";

const Checkout = () => {
    let cartTotalPrice = 0;
    let { pathname } = useLocation();
    const currency = useSelector((state) => state.currency);
    const { cartItems } = useSelector((state) => state.cart);
    const [cartDetails, setCartDetails] = useState([]);
    const [taxes, setTaxes] = useState([]);
    const [paymentSettings, setPaymentSettings] = useState({});
    const [Razorpay] = useRazorpay();
    const sessionId = localStorage.getItem("sessionId");

    // Get cart details by sessionId
    const getCartDetails = async () => {
        const sessionId = localStorage.getItem("sessionId");

        try {
            const response = await axios.get(
                `${APP_URL}/carts/details/sessionID/${sessionId}`
            );
            setCartDetails(response.data.cart[0]);
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // Get taxes
    const getTaxes = async () => {
        try {
            const response = await axios.get(`${APP_URL}/get-taxes`);
            setTaxes(response.data.taxes);
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    // Get payment methods
    const getPaymentMethods = async () => {
        try {
            const response = await axios.get(`${APP_URL}/get-payment-methods`);
            setPaymentSettings(response.data.data);
        } catch (error) {
            console.log(error);
        }
    };

    // Place order
    const placeOrder = async (method) => {
        try {
            if (method === "razorpay") {
                await handleRazorpayPayment();
                return;
            } else if (method === "COD") {
                await saveOrderAndCreateInvoice("COD", {
                    status: "Completed",
                    amount: cartDetails.total,
                });
                return;
            }
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // Save order
    const saveOrderAndCreateInvoice = async (method, orderData) => {
        try {
            const placeOrderResponse = await axios.post(
                `${APP_URL}/orders/store/${sessionId}`
            );

            orderData.paymentMethod = method; // Add payment method to orderData
            orderData.orderId = placeOrderResponse.data.data.order._id; // Add orderId to orderData

            const saveOrderResponse = await axios.post(
                `${APP_URL}/payments/store`,
                orderData
            );

            if (
                placeOrderResponse.data.status === "success" &&
                saveOrderResponse.data.status === "success"
            ) {
                clearStorage();
            }
        } catch (error) {
            console.log(error);
        }
    };

    const createRazorpayOrder = async () => {
        let response;

        const data = {
            amount: cartDetails.total * 100,
            currency: "INR",
        };

        try {
            response = await axios.post(`${APP_URL}/create-order`, data);
        } catch (error) {
            console.log(error);
        }

        return response;
    };

    const handleRazorpayPayment = async () => {
        let orderData = {};

        try {
            var order = await createRazorpayOrder();
            order = order.data;
        } catch (error) {
            console.log(error);
        }

        var options = {
            key: paymentSettings.razorpayKey,
            amount: order.amount,
            currency: order.currency,
            name: "Test",
            description: "Test Transaction",
            order_id: order.id,
            image: "https://img.freepik.com/free-vector/bird-colorful-gradient-design-vector_343694-2506.jpg",
            handler: async (response) => {
                orderData = {
                    status: "Completed",
                    amount: cartDetails.total,
                    paymentMethod: "razorpay",
                    razorpayPaymentId: response.razorpay_payment_id,
                    razorpayOrderId: response.razorpay_order_id,
                    razorpaySignature: response.razorpay_signature,
                };

                await saveOrderAndCreateInvoice("razorpay", orderData);
            },
            prefill: {
                name: cartDetails.user[0].name,
                email: cartDetails.user[0].email,
                contact: cartDetails.user[0].mobile,
            },
            notes: {
                address: cartDetails.deliveryInstruction,
            },
            theme: {
                color: "#3399cc",
            },
        };

        try {
            var rzpay = new Razorpay(options);
        } catch (error) {
            console.log(error);
        }

        rzpay.on("payment.failed", async function (response) {
            orderData = {
                status: "Failed",
                amount: cartDetails.total,
                paymentMethod: "razorpay",
                razorpayPaymentId: response.error.metadata.payment_id,
                razorpayOrderId: response.error.metadata.order_id,
                razorpaySignature: response.error.reason,
            };

            await saveOrderAndCreateInvoice("razorpay", orderData);
        });

        rzpay.open();
    };

    // Clear localStorage after successful payment capture
    const clearStorage = () => {
        const newSessionId = Math.random().toString(36).substring(2, 15); // Generate new sessionId

        localStorage.setItem("sessionId", newSessionId); // Reset sessionId

        // Remove persist:flone from local storage
        localStorage.removeItem("persist:flone");

        cogoToast.success("Payment received successfully.", {
            position: "bottom-left",
        });

        setTimeout(() => {
            window.location.href = process.env.PUBLIC_URL + "/";
        }, 2000);
    };

    useEffect(() => {
        getCartDetails();
        getTaxes();
        getPaymentMethods();
    }, []);

    return (
        <>
            <SEO
                titleTemplate="Checkout"
                description="Checkout page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Cart",
                            path: process.env.PUBLIC_URL + "/cart",
                        },
                        {
                            label: "Checkout",
                            path: process.env.PUBLIC_URL + "/checkout",
                        },
                        {
                            label: "Payment",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="checkout-area pt-95 pb-100">
                    <div className="container">
                        {cartItems && cartItems.length >= 1 ? (
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="your-order-area">
                                        <h3>Order Summery</h3>
                                        <div className="your-order-wrap gray-bg-4">
                                            <div className="your-order-product-info">
                                                <div className="your-order-top">
                                                    <ul>
                                                        <li>Product</li>
                                                        <li>Total</li>
                                                    </ul>
                                                </div>
                                                <div className="your-order-middle">
                                                    <ul>
                                                        {cartItems.map(
                                                            (cartItem, key) => {
                                                                const discountedPrice =
                                                                    getDiscountPrice(
                                                                        cartItem.price,
                                                                        cartItem.discount
                                                                    );
                                                                const finalProductPrice =
                                                                    (
                                                                        cartItem.price *
                                                                        currency.currencyRate
                                                                    ).toFixed(
                                                                        2
                                                                    );
                                                                const finalDiscountedPrice =
                                                                    (
                                                                        discountedPrice *
                                                                        currency.currencyRate
                                                                    ).toFixed(
                                                                        2
                                                                    );

                                                                discountedPrice !=
                                                                null
                                                                    ? (cartTotalPrice +=
                                                                          finalDiscountedPrice *
                                                                          cartItem.quantity)
                                                                    : (cartTotalPrice +=
                                                                          finalProductPrice *
                                                                          cartItem.quantity);
                                                                return (
                                                                    <li
                                                                        key={
                                                                            key
                                                                        }
                                                                    >
                                                                        <span className="order-middle-left">
                                                                            {
                                                                                cartItem.name
                                                                            }{" "}
                                                                            X{" "}
                                                                            {
                                                                                cartItem.quantity
                                                                            }
                                                                        </span>{" "}
                                                                        <span className="order-price">
                                                                            {discountedPrice !==
                                                                            null
                                                                                ? currency.currencySymbol +
                                                                                  (
                                                                                      finalDiscountedPrice *
                                                                                      cartItem.quantity
                                                                                  ).toFixed(
                                                                                      2
                                                                                  )
                                                                                : currency.currencySymbol +
                                                                                  (
                                                                                      finalProductPrice *
                                                                                      cartItem.quantity
                                                                                  ).toFixed(
                                                                                      2
                                                                                  )}
                                                                        </span>
                                                                    </li>
                                                                );
                                                            }
                                                        )}
                                                    </ul>
                                                </div>
                                                <div className="your-order-top">
                                                    <ul>
                                                        <li>Taxes</li>
                                                        <li>Total</li>
                                                    </ul>
                                                </div>
                                                <div className="your-order-middle">
                                                    <ul>
                                                        {taxes.map(
                                                            (tax, key) => {
                                                                return (
                                                                    <li
                                                                        key={
                                                                            key
                                                                        }
                                                                    >
                                                                        <span className="order-middle-left">
                                                                            {
                                                                                tax.name
                                                                            }
                                                                        </span>{" "}
                                                                        <span className="order-price">
                                                                            {currency.currencySymbol +
                                                                                (cartDetails &&
                                                                                    (
                                                                                        (cartDetails.subTotal *
                                                                                            tax.rate) /
                                                                                        100
                                                                                    ).toFixed(
                                                                                        2
                                                                                    ))}
                                                                        </span>
                                                                    </li>
                                                                );
                                                            }
                                                        )}
                                                    </ul>
                                                </div>

                                                {cartDetails &&
                                                cartDetails.discount > 0 ? (
                                                    <div className="your-order-bottom">
                                                        <ul>
                                                            <li className="your-order-shipping">
                                                                Coupon Applied
                                                            </li>
                                                            <li>
                                                                {currency.currencySymbol +
                                                                    (cartDetails &&
                                                                    cartDetails.discount
                                                                        ? cartDetails.discount
                                                                        : 0)}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                ) : null}

                                                <div className="your-order-total">
                                                    <ul>
                                                        <li className="order-total">
                                                            Total
                                                        </li>
                                                        <li>
                                                            {currency.currencySymbol +
                                                                (cartDetails &&
                                                                cartDetails.total
                                                                    ? cartDetails.total
                                                                    : 0)}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="payment-method"></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="your-order-area payment-methods-container">
                                        <h3>Payment Methods</h3>
                                        <div className="payment-methods-buttons">
                                            {paymentSettings &&
                                            paymentSettings.razorpayStatus ? (
                                                <button
                                                    className="payment-button"
                                                    onClick={() =>
                                                        placeOrder("razorpay")
                                                    }
                                                >
                                                    <svg
                                                        role="img"
                                                        width="16"
                                                        height="16"
                                                        viewBox="0 0 24 24"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <title>Razorpay</title>
                                                        <path d="M22.436 0l-11.91 7.773-1.174 4.276 6.625-4.297L11.65 24h4.391l6.395-24zM14.26 10.098L3.389 17.166 1.564 24h9.008l3.688-13.902Z" />
                                                    </svg>{" "}
                                                    Pay with Razorpay
                                                </button>
                                            ) : null}

                                            {paymentSettings &&
                                            paymentSettings.paypalStatus ? (
                                                <button
                                                    className="payment-button"
                                                    onClick={() =>
                                                        placeOrder("paypal")
                                                    }
                                                >
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="16"
                                                        height="16"
                                                        fill="currentColor"
                                                        class="bi bi-paypal"
                                                        viewBox="0 0 16 16"
                                                    >
                                                        <path d="M14.06 3.713c.12-1.071-.093-1.832-.702-2.526C12.628.356 11.312 0 9.626 0H4.734a.7.7 0 0 0-.691.59L2.005 13.509a.42.42 0 0 0 .415.486h2.756l-.202 1.28a.628.628 0 0 0 .62.726H8.14c.429 0 .793-.31.862-.731l.025-.13.48-3.043.03-.164.001-.007a.35.35 0 0 1 .348-.297h.38c1.266 0 2.425-.256 3.345-.91q.57-.403.993-1.005a4.94 4.94 0 0 0 .88-2.195c.242-1.246.13-2.356-.57-3.154a2.7 2.7 0 0 0-.76-.59l-.094-.061ZM6.543 8.82a.7.7 0 0 1 .321-.079H8.3c2.82 0 5.027-1.144 5.672-4.456l.003-.016q.326.186.548.438c.546.623.679 1.535.45 2.71-.272 1.397-.866 2.307-1.663 2.874-.802.57-1.842.815-3.043.815h-.38a.87.87 0 0 0-.863.734l-.03.164-.48 3.043-.024.13-.001.004a.35.35 0 0 1-.348.296H5.595a.106.106 0 0 1-.105-.123l.208-1.32z" />
                                                    </svg>{" "}
                                                    Pay with Paypal
                                                </button>
                                            ) : null}

                                            {paymentSettings &&
                                            paymentSettings.stripeStatus ? (
                                                <button
                                                    className="payment-button"
                                                    onClick={() =>
                                                        placeOrder("stripe")
                                                    }
                                                >
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 256 256"
                                                        width="16"
                                                        height="16"
                                                    >
                                                        <rect
                                                            width="256"
                                                            height="256"
                                                            fill="none"
                                                        />
                                                        <path
                                                            d="M91.9,152c0,13.3,16.2,24,36.1,24s36.1-10.7,36.1-24c0-32-70.2-20-70.2-48,0-13.3,14.2-24,34.1-24,14.9,0,26.6,6,31.5,14.7"
                                                            fill="none"
                                                            stroke="#000"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="24"
                                                        />
                                                        <rect
                                                            x="40"
                                                            y="40"
                                                            width="176"
                                                            height="176"
                                                            rx="8"
                                                            fill="none"
                                                            stroke="#000"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="24"
                                                        />
                                                    </svg>{" "}
                                                    Pay with Stripe
                                                </button>
                                            ) : null}

                                            {/* Cash on Delivery */}
                                            <button
                                                className="payment-button"
                                                onClick={() =>
                                                    placeOrder("COD")
                                                }
                                            >
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 576 512"
                                                    width="16"
                                                    height="16"
                                                >
                                                    <path d="M64 64C28.7 64 0 92.7 0 128V384c0 35.3 28.7 64 64 64H512c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H64zm64 320H64V320c35.3 0 64 28.7 64 64zM64 192V128h64c0 35.3-28.7 64-64 64zM448 384c0-35.3 28.7-64 64-64v64H448zm64-192c-35.3 0-64-28.7-64-64h64v64zM288 160a96 96 0 1 1 0 192 96 96 0 1 1 0-192z" />
                                                </svg>{" "}
                                                Cash on Delivery
                                            </button>
                                            {/* Cash on Delivery */}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 text-center mt-5">
                                    <div className="item-empty-area__text">
                                        <Link
                                            to={
                                                process.env.PUBLIC_URL +
                                                "/checkout"
                                            }
                                        >
                                            Go back to checkout
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="item-empty-area text-center">
                                        <div className="item-empty-area__icon mb-30">
                                            <i className="pe-7s-cash"></i>
                                        </div>
                                        <div className="item-empty-area__text">
                                            No items found in cart to checkout{" "}
                                            <br />{" "}
                                            <Link
                                                to={
                                                    process.env.PUBLIC_URL +
                                                    "/shop"
                                                }
                                            >
                                                Shop Now
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </LayoutOne>
        </>
    );
};

export default Checkout;
