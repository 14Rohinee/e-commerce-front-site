import React from "react";
import { Link, useLocation } from "react-router-dom";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";

const Login = () => {
    let { pathname } = useLocation();

    // Login functionality
    const registerSubmit = (e) => {
        e.preventDefault();
    };

    return (
        <>
            <SEO
                titleTemplate="Login"
                description="Login page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Sign Up",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="login-register-area pt-50 pb-70">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-md-12 ms-auto me-auto">
                                <h4 className="text-center mb-3 login-register-heading-text">Sign Up</h4>
                                <div className="login-register-wrapper">
                                    <div className="login-form-container">
                                        <div className="login-register-form">
                                            <form>
                                                <input
                                                    type="text"
                                                    name="user-name"
                                                    placeholder="Username"
                                                />
                                                <input
                                                    type="password"
                                                    name="user-password"
                                                    placeholder="Password"
                                                />
                                                <input
                                                    name="user-email"
                                                    placeholder="Email"
                                                    type="email"
                                                />
                                                <div className="button-box">
                                                    <button type="submit">
                                                        <span>Sign Up</span>
                                                    </button>
                                                </div>

                                                <div className="text-center">
                                                    <p>
                                                        Already have an account?{" "}
                                                        <Link
                                                            to={
                                                                process.env
                                                                    .PUBLIC_URL +
                                                                "/login"
                                                            }
                                                        >
                                                            Sign In
                                                        </Link>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutOne>
        </>
    );
};

export default Login;
