import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { getDiscountPrice } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import cogoToast from "cogo-toast";
import axios from "axios";
import AddressModal from "../../components/address/AddressModal";
import { APP_URL } from "../../helpers/product";

const Checkout = () => {
    let cartTotalPrice = 0;
    let { pathname } = useLocation();
    const currency = useSelector((state) => state.currency);
    const { cartItems } = useSelector((state) => state.cart);
    const [selectedAddress, setSelectedAddress] = useState("");
    const [shippingAddresses, setShippingAddresses] = useState([]);
    const [isAddressModalOpen, setIsAddressModalOpen] = useState(false);
    const [cartDetails, setCartDetails] = useState([]);
    const [taxes, setTaxes] = useState([]);
    const [couponCode, setCouponCode] = useState("");
    const [deliveryInstruction, setDeliveryInstruction] = useState("");
    const user = JSON.parse(localStorage.getItem("loggedInUser"));
    const sessionId = localStorage.getItem("sessionId");

    const handleAddressClick = (addressId) => {
        setSelectedAddress(addressId);
    };

    const addNewAddressClick = () => {
        setIsAddressModalOpen(true);
    };

    // Get addresses
    const getAddresses = async () => {
        try {
            const response = await axios.get(
                `${APP_URL}/shipping-addresses/${user._id}`
            );
            setShippingAddresses(response.data.data.result);

            // Loop through the addresses and select the default address
            response.data.data.result.map((address) => {
                if (address.isDefaultAddress) {
                    setSelectedAddress(address._id);
                }
            });
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    const closeAddressModal = () => {
        getAddresses();
        setIsAddressModalOpen(false);
    };

    // Get cart details by sessionId
    const getCartDetails = async () => {
        const sessionId = localStorage.getItem("sessionId");

        try {
            const response = await axios.get(
                `${APP_URL}/carts/details/sessionID/${sessionId}`
            );
            setCartDetails(response.data.cart[0]);
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // Get taxes
    const getTaxes = async () => {
        try {
            const response = await axios.get(`${APP_URL}/get-taxes`);
            setTaxes(response.data.taxes);
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // Apply coupon code
    const applyCouponCode = async () => {
        const sessionId = localStorage.getItem("sessionId");

        try {
            const response = await axios.post(`${APP_URL}/apply-coupon`, {
                sessionId,
                couponCode,
            });
            setCartDetails(response.data.cart[0]);
            setCouponCode("");
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // remove coupon
    const removeCoupon = async () => {
        try {
            const response = await axios.post(`${APP_URL}/remove-coupon`, {
                sessionId,
            });
            setCartDetails(response.data.cart[0]);
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    // Proceed to payment
    const proceedToPayment = async () => {
        // Check if shippingAddresses is empty
        if (shippingAddresses.length === 0) {
            cogoToast.error("Please add an address", {
                position: "bottom-left",
            });
            return;
        }

        // Validate if an address is selected
        if (selectedAddress === "") {
            cogoToast.error("Please select an address", {
                position: "bottom-left",
            });
            return;
        }

        try {
            const response = await axios.post(
                `${APP_URL}/carts/save-checkout-data`,
                {
                    sessionId,
                    userId: user._id,
                    shippingAddressId: selectedAddress,
                    deliveryInstruction,
                }
            );
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });

            setTimeout(() => {
                window.location.href = "/payment";
            }, 2000);
        } catch (error) {
            console.log(error.response.data.message);
        }
    };

    useEffect(() => {
        getAddresses();
        getCartDetails();
        getTaxes();
    }, []);

    return (
        <>
            <SEO
                titleTemplate="Checkout"
                description="Checkout page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Cart",
                            path: process.env.PUBLIC_URL + "/cart",
                        },
                        {
                            label: "Checkout",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="checkout-area pt-95 pb-100">
                    <div className="container">
                        {cartItems && cartItems.length >= 1 ? (
                            <>
                                <div className="row">
                                    <div className="col-lg-7">
                                        <div className="billing-info-wrap">
                                            <h3>Billing Details</h3>

                                            {/* Create a radio option div to select one of the pre-saved addresses */}
                                            <div className="row">
                                                {shippingAddresses &&
                                                    shippingAddresses.map(
                                                        (address, key) => {
                                                            return (
                                                                <div
                                                                    className="col-lg-6 col-sm-12"
                                                                    key={key}
                                                                >
                                                                    <div
                                                                        className={`address-card ${
                                                                            address._id ===
                                                                            selectedAddress
                                                                                ? "selected"
                                                                                : ""
                                                                        }`}
                                                                        onClick={() =>
                                                                            handleAddressClick(
                                                                                address._id
                                                                            )
                                                                        }
                                                                    >
                                                                        <ul>
                                                                            <li>
                                                                                <h4>
                                                                                    {
                                                                                        address.name
                                                                                    }
                                                                                </h4>
                                                                            </li>
                                                                            <li>
                                                                                <p>
                                                                                    {
                                                                                        address.houseNumber
                                                                                    }
                                                                                </p>
                                                                            </li>
                                                                            <li>
                                                                                <p>
                                                                                    {
                                                                                        address.address
                                                                                    }
                                                                                </p>
                                                                            </li>
                                                                            <li>
                                                                                <p>
                                                                                    {
                                                                                        address.landmark
                                                                                    }
                                                                                </p>
                                                                            </li>
                                                                            <li>
                                                                                <p>
                                                                                    {`${address.city} ${address.state} ${address.country}, ${address.zip} `}
                                                                                </p>
                                                                            </li>
                                                                            <li>
                                                                                <p>
                                                                                    {`Phone Number : ${address.phone}`}
                                                                                </p>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            );
                                                        }
                                                    )}
                                            </div>

                                            <div className="row">
                                                <div className="col-lg-12">
                                                    {/* Add new address */}
                                                    <div className="add-new-address">
                                                        <a
                                                            href="#"
                                                            onClick={
                                                                addNewAddressClick
                                                            }
                                                        >
                                                            Add New Address
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="additional-info-wrap mt-4">
                                                <h4>Additional information</h4>
                                                <div className="additional-info">
                                                    <label>
                                                        Delivery Instruction
                                                    </label>
                                                    <textarea
                                                        placeholder="Notes about your order, e.g. special notes for delivery. "
                                                        name="message"
                                                        defaultValue={
                                                            deliveryInstruction
                                                        }
                                                        onChange={(e) =>
                                                            setDeliveryInstruction(
                                                                e.target.value
                                                            )
                                                        }
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5">
                                        <div className="your-order-area">
                                            <h3>Order Summery</h3>
                                            <div className="your-order-wrap gray-bg-4">
                                                <div className="your-order-product-info">
                                                    <div className="your-order-top">
                                                        <ul>
                                                            <li>Product</li>
                                                            <li>Total</li>
                                                        </ul>
                                                    </div>
                                                    <div className="your-order-middle">
                                                        <ul>
                                                            {cartItems.map(
                                                                (
                                                                    cartItem,
                                                                    key
                                                                ) => {
                                                                    const discountedPrice =
                                                                        getDiscountPrice(
                                                                            cartItem.price,
                                                                            cartItem.discount
                                                                        );
                                                                    const finalProductPrice =
                                                                        (
                                                                            cartItem.price *
                                                                            currency.currencyRate
                                                                        ).toFixed(
                                                                            2
                                                                        );
                                                                    const finalDiscountedPrice =
                                                                        (
                                                                            discountedPrice *
                                                                            currency.currencyRate
                                                                        ).toFixed(
                                                                            2
                                                                        );

                                                                    discountedPrice !=
                                                                    null
                                                                        ? (cartTotalPrice +=
                                                                              finalDiscountedPrice *
                                                                              cartItem.quantity)
                                                                        : (cartTotalPrice +=
                                                                              finalProductPrice *
                                                                              cartItem.quantity);
                                                                    return (
                                                                        <li
                                                                            key={
                                                                                key
                                                                            }
                                                                        >
                                                                            <span className="order-middle-left">
                                                                                {
                                                                                    cartItem.name
                                                                                }{" "}
                                                                                X{" "}
                                                                                {
                                                                                    cartItem.quantity
                                                                                }
                                                                            </span>{" "}
                                                                            <span className="order-price">
                                                                                {discountedPrice !==
                                                                                null
                                                                                    ? currency.currencySymbol +
                                                                                      (
                                                                                          finalDiscountedPrice *
                                                                                          cartItem.quantity
                                                                                      ).toFixed(
                                                                                          2
                                                                                      )
                                                                                    : currency.currencySymbol +
                                                                                      (
                                                                                          finalProductPrice *
                                                                                          cartItem.quantity
                                                                                      ).toFixed(
                                                                                          2
                                                                                      )}
                                                                            </span>
                                                                        </li>
                                                                    );
                                                                }
                                                            )}
                                                        </ul>
                                                    </div>
                                                    <div className="your-order-top">
                                                        <ul>
                                                            <li>Taxes</li>
                                                            <li>Total</li>
                                                        </ul>
                                                    </div>
                                                    <div className="your-order-middle remove-border-and-padding-bottom">
                                                        <ul>
                                                            {taxes.map(
                                                                (tax, key) => {
                                                                    return (
                                                                        <li
                                                                            key={
                                                                                key
                                                                            }
                                                                        >
                                                                            <span className="order-middle-left">
                                                                                {
                                                                                    tax.name
                                                                                }
                                                                            </span>{" "}
                                                                            <span className="order-price">
                                                                                {currency.currencySymbol +
                                                                                    (cartDetails &&
                                                                                    (
                                                                                        (cartDetails.subTotal *
                                                                                            tax.rate) /
                                                                                        100
                                                                                    ).toFixed(
                                                                                        2
                                                                                    ))}
                                                                            </span>
                                                                        </li>
                                                                    );
                                                                }
                                                            )}
                                                        </ul>
                                                    </div>

                                                    {cartDetails &&
                                                    cartDetails.discount > 0 ? (
                                                        <div className="your-order-bottom add-border-and-padding-top">
                                                            <ul className="coupon">
                                                                <li className="your-order-shipping">
                                                                    <div>
                                                                        <p className="mb-0">
                                                                            <span className="coupon-text">
                                                                                OFF10
                                                                            </span>
                                                                            {" "}
                                                                            <span>
                                                                                applied
                                                                            </span>
                                                                        </p>
                                                                        <p className="coupon-value">
                                                                            -
                                                                            {
                                                                                currency.currencySymbol +
                                                                                (cartDetails && cartDetails.discount ? (cartDetails.discount).toFixed(2) : 0)
                                                                            }
                                                                            {" "}
                                                                            (10% off)
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <a href="#" onClick={removeCoupon} className="text-danger">
                                                                        Remove
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    ) : null}

                                                    <div className="your-order-total">
                                                        <ul>
                                                            <li className="order-total">
                                                                Total
                                                            </li>
                                                            <li>
                                                                {
                                                                    currency.currencySymbol +
                                                                    (cartDetails && cartDetails.total ? (cartDetails.total).toFixed(2) : 0)
                                                                }
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="payment-method"></div>
                                            </div>

                                            {cartDetails &&
                                            cartDetails.discount > 0 ? (
                                                ""
                                            ) : (
                                                <>
                                                    <div className="discount-code-wrapper mt-4">
                                                        <div className="title-wrap">
                                                            <h4 className="cart-bottom-title section-bg-gray">
                                                                Use Coupon Code
                                                            </h4>
                                                        </div>
                                                        <div className="discount-code billing-info-wrap">
                                                            <p>
                                                                Enter your
                                                                coupon code if
                                                                you have one.
                                                            </p>
                                                            <div className="input-group d-block">
                                                                <input
                                                                    type="text"
                                                                    placeholder="Coupon code"
                                                                    value={
                                                                        couponCode
                                                                    }
                                                                    onChange={(
                                                                        e
                                                                    ) =>
                                                                        setCouponCode(
                                                                            e
                                                                                .target
                                                                                .value
                                                                        )
                                                                    }
                                                                />
                                                                <button
                                                                    className="btn"
                                                                    type="button"
                                                                    onClick={
                                                                        applyCouponCode
                                                                    }
                                                                >
                                                                    Apply
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-5">
                                    <div className="col-auto">
                                        <Link
                                            className="secondary-btn"
                                            to={
                                                process.env.PUBLIC_URL + "/cart"
                                            }
                                        >
                                            Cart
                                        </Link>
                                    </div>
                                    <div className="col-auto ms-auto">
                                        <Link
                                            className="primary-btn"
                                            onClick={proceedToPayment}
                                        >
                                            Payment
                                        </Link>
                                    </div>
                                </div>
                            </>
                        ) : (
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="item-empty-area text-center">
                                        <div className="item-empty-area__icon mb-30">
                                            <i className="pe-7s-cash"></i>
                                        </div>
                                        <div className="item-empty-area__text">
                                            No items found in cart to checkout{" "}
                                            <br />{" "}
                                            <Link
                                                to={
                                                    process.env.PUBLIC_URL +
                                                    "/shop"
                                                }
                                            >
                                                Shop Now
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>

                <AddressModal
                    show={isAddressModalOpen}
                    onHide={closeAddressModal}
                />
            </LayoutOne>
        </>
    );
};

export default Checkout;
