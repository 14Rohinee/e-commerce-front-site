import React from "react";
import { Navigate } from "react-router-dom";

const ProtectedRoute = ({ children, isLoggedIn, redirectTo }) => {
    if (!isLoggedIn) {
        return <Navigate to={`/login?redirect=${redirectTo}`} />;
    }

    return children;
};

export default ProtectedRoute;
