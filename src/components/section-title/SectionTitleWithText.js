import PropTypes from "prop-types";
import clsx from "clsx";

const SectionTitleWithText = ({ spaceTopClass, spaceBottomClass }) => {
  return (
    <div className={clsx("welcome-area", spaceTopClass, spaceBottomClass)}>
      <div className="container">
        <div className="welcome-content text-center">
          <h1>Who Am I</h1>
          <p>
            I'm Rohinee, a full stack developer with 2 and half years of experience in web development. This project serves as a showcase for my portfolio, demonstrating my proficiency in utilizing cutting-edge technologies to create a comprehensive eCommerce platform.
          </p>
        </div>
      </div>
    </div>
  );
};

SectionTitleWithText.propTypes = {
  spaceBottomClass: PropTypes.string,
  spaceTopClass: PropTypes.string
};

export default SectionTitleWithText;
