import axios from "axios";
import cogoToast from "cogo-toast";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { APP_URL } from "../../helpers/product";

function AddressModal({ addressId, show, onHide }) {
    const [addressData, setAddressData] = useState({
        userId:
            localStorage.getItem("loggedInUser") &&
            JSON.parse(localStorage.getItem("loggedInUser"))._id,
        name: "",
        email: "",
        phone: "",
        address: "",
        landmark: "",
        houseNumber: "",
        city: "",
        state: "",
        zip: "",
        isDefaultAddress: false,
    });

    // Get address details
    const getAddressDetails = async () => {
        // Update address data
        try {
            const response = await axios.get(
                `${APP_URL}/shipping-addresses/edit/${addressId}`
            );
            setAddressData(response.data.data.shippingAddress.shippingAddress);
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    const onCloseModal = () => {
        onHide();
    };

    // onAddressSave
    const updateAddress = async () => {
        // Save data
        try {
            const response = await axios.put(`${APP_URL}/shipping-addresses/update/${addressId}`, addressData);
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });

            // Close modal
            onCloseModal();
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    // saveAddress
    const saveAddress = async () => {
        // Update address data
        try {
            const response = await axios.post(`${APP_URL}/shipping-addresses/store`, addressData);
            cogoToast.success(response.data.message, {
                position: "bottom-left",
            });

            // Close modal
            onCloseModal();
        } catch (error) {
            cogoToast.error(error.response.data.message, {
                position: "bottom-left",
            });
        }
    };

    useEffect(() => {
        // Check if addressId is not null
        if (addressId) {
            getAddressDetails();
        }
    }, [addressId]);

    return (
        <Modal show={show} onHide={onCloseModal} className="">
            <Modal.Header closeButton>
                <Modal.Title>
                    {addressId ? "Edit Address" : "Add New Address"}
                </Modal.Title>
            </Modal.Header>

            <div className="modal-body">
                <div className="checkout-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12">
                                <div className="billing-info mb-20">
                                    <label>Name</label>
                                    <input
                                        type="text"
                                        value={addressData.name}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                name: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="billing-info mb-20">
                                    <label>Street Address</label>
                                    <input
                                        className="billing-address"
                                        placeholder="House number"
                                        type="text"
                                        value={addressData.houseNumber}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                houseNumber: e.target.value,
                                            });
                                        }}
                                    />
                                    <input
                                        placeholder="Apartment, suite, unit, landmark etc."
                                        type="text"
                                        value={addressData.address}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                address: e.target.value,
                                            });
                                        }}
                                    />
                                    <input
                                        placeholder="Near by landmark"
                                        type="text"
                                        value={addressData.landmark}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                landmark: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="billing-info mb-20">
                                    <label>Town / City</label>
                                    <input
                                        type="text"
                                        value={addressData.city}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                city: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="billing-info mb-20">
                                    <label>State</label>
                                    <input
                                        type="text"
                                        value={addressData.state}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                state: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="billing-info mb-20">
                                    <label>Postcode / ZIP</label>
                                    <input
                                        type="text"
                                        value={addressData.zip}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                zip: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="billing-info mb-20">
                                    <label>Phone</label>
                                    <input
                                        type="text"
                                        value={addressData.phone}
                                        onChange={(e) => {
                                            setAddressData({
                                                ...addressData,
                                                phone: e.target.value,
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="">
                                    <label>Set as Default</label>
                                    <select
                                        onChange={(e) =>
                                            setAddressData((prev) => ({
                                                ...prev,
                                                isDefaultAddress:
                                                    e.target.value,
                                            }))
                                        }
                                    >
                                        <option value="">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="billing-info mb-20">
                                    {
                                        addressId ? (
                                            <button
                                                className="primary-btn"
                                                onClick={updateAddress}
                                            >
                                                Update Address
                                            </button>
                                        ) : (
                                            <button
                                                className="primary-btn"
                                                onClick={saveAddress}
                                            >
                                                Save Address
                                            </button>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}

AddressModal.propTypes = {
    address: PropTypes.object,
    show: PropTypes.bool,
    onHide: PropTypes.func,
};

export default AddressModal;
